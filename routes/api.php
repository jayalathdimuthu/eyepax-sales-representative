<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\SalesRepresentativeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/sales-representative', [SalesRepresentativeController::class, 'index'])->name('sales-representative.index');
Route::post('/sales-representative', [SalesRepresentativeController::class, 'store'])->name('sales-representative.store');
Route::delete('/sales-representative', [SalesRepresentativeController::class, 'destroy'])->name('sales-representative.destroy');
