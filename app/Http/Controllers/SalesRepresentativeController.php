<?php

namespace App\Http\Controllers;

use App\domain\SalesRepresentative\Actions\CreateSalesRepresentativeAction;
use App\domain\SalesRepresentative\DataTransferObjects\SalesRepresentativeFormData;
use App\Http\Requests\SalesRepresentativeFormRequest;
use App\Models\SalesRepresentative;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SalesRepresentativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(SalesRepresentative::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SalesRepresentativeFormRequest $salesRepresentativeFormRequest
     * @param CreateSalesRepresentativeAction $createSalesRepresentativeAction
     *
     * @return JsonResponse
     */
    public function store(
        SalesRepresentativeFormRequest $salesRepresentativeFormRequest,
        CreateSalesRepresentativeAction $createSalesRepresentativeAction
    ): JsonResponse {
        try {
            return response()->json(
                $createSalesRepresentativeAction(
                    new SalesRepresentativeFormData(
                        name: $salesRepresentativeFormRequest->name,
                        email: $salesRepresentativeFormRequest->email,
                        telephone: $salesRepresentativeFormRequest->telephone,
                        currentRoute: $salesRepresentativeFormRequest->currentRoute,
                        joinDate: $salesRepresentativeFormRequest->joinDate,
                        comments: $salesRepresentativeFormRequest->comments,
                    )
                ),
                Response::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalesRepresentative  $salesRepresentative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesRepresentative $salesRepresentative)
    {
        $salesRepresentative->update([
            'name' => $request->name,
            'email' => $request->email,
            'telephone' => $request->telephone,
            'current_route' => $request->currentRoute,
            'join_date' => $request->joinDate,
            'comments' => $request->comments,
        ]);

        return response()->json($salesRepresentative);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalesRepresentative  $salesRepresentative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $salesRepresentative = SalesRepresentative::find($request->id);
        $salesRepresentative->delete();
    }
}
