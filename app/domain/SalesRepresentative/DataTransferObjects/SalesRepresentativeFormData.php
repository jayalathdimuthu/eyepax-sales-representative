<?php

namespace App\domain\SalesRepresentative\DataTransferObjects;

use Illuminate\Support\Facades\Date;
use Spatie\DataTransferObject\DataTransferObject;

class SalesRepresentativeFormData extends DataTransferObject
{
    /**
     * Sales Representative id of the Sales Representatives.
     *
     * @var int|null
     */
    public ?int $id;

    /**
     * Sales Representative name of the Sales Representatives.
     *
     * @var string|null
     */
    public ?string $name;

    /**
     * Sales Representative email of the Sales Representatives.
     *
     * @var string|null
     */
    public ?string $email;


    /**
     * Sales Representative telephone of the Sales Representatives.
     *
     * @var string|null
     */
    public ?string $telephone;

    /**
     * Sales Representative join date of the Sales Representatives.
     *
     * @var date|null
     */

    public ?Date $joinDate;

    /**
     * Sales Representative current route of the Sales Representatives.
     *
     * @var string|null
     */
    public ?string $currentRoute;

    /**
     * Sales Representative comments of the Sales Representatives.
     *
     * @var string|null
     */
    public ?string $comments;
}
