<?php

namespace App\domain\SalesRepresentative\Actions;

use App\domain\SalesRepresentative\DataTransferObjects\SalesRepresentativeFormData;
use App\Http\Resources\SalesRepresentativeResource;
use App\Models\SalesRepresentative;
use Exception;
use Illuminate\Support\Facades\DB;

class CreateSalesRepresentativeAction
{
    /**
     * Store Sales Representative action.
     *
     * @throws Exception
     */
    public function __invoke(SalesRepresentativeFormData $salesRepresentativeFormData): SalesRepresentativeResource
    {
        try {
            DB::beginTransaction();

            /** @var SalesRepresentative $salesRepresentative */
            $salesRepresentative = SalesRepresentative::create([
                'name' => $salesRepresentativeFormData->name,
                'email' => $salesRepresentativeFormData->email,
                'telephone' => $salesRepresentativeFormData->telephone,
                'current_route' => $salesRepresentativeFormData->currentRoute,
                'join_date' => $salesRepresentativeFormData->joinDate,
                'comments' => $salesRepresentativeFormData->comments,
            ]);

            DB::commit();

            return new SalesRepresentativeResource($salesRepresentative);
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}
